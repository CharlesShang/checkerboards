load('../Checkerboards_LDCF_codebase/models_Caltech/Checkerboards/CheckeboardsDetector.mat');
detector = acfModify(detector,'cascThr',-1,'cascCal',0.05);

vbbDir='~/Work/CheckerBoards/data/Caltech/';
testdataDir = '~/Work/CheckerBoards/data/Caltech/train/images/';
testgtDir = '~/Work/CheckerBoards/data/Caltech/train/annotations/';

pLoad={'lbls',{'person'},'ilbls',{'people'},'squarify',{3,.41}};

%% some exmaples 
dataDir = '~/Work/CheckerBoards/data/Caltech/';
imgNms=bbGt('getFiles',{[dataDir 'test/images']});
I=imread(imgNms{1862}); 
tic, bbs=acfDetect(I,detector); toc
figure(1); im(I); bbApply('draw',bbs); pause(.1);
I=imread(imgNms{2588}); 
tic, bbs=acfDetect(I,detector); toc
figure(1); im(I); bbApply('draw',bbs); pause(.1);
% some exmaples end

tstart = tic;
[miss,gt,dt]=sampleNegWidowsCaltech(vbbDir, detector, ...
     'reapply', 1, ...
      'detres','CheckerboardsWindows.txt','imgDir',testdataDir ,...
      'gtDir',testgtDir,'pLoad',[pLoad, 'hRng',[50 inf],...
      'vRng',[.65 1],'xRng',[5 635],'yRng',[5 475]],'show',2);
telapsed = toc(tstart);