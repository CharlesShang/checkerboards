Fork from ShanShan's repo

This is the code release of the work described in the following papers:

Filtered Channel Features for Pedestrian Detection. 
Shanshan Zhang, Rodrigo Benenson, Bernt Schiele. 
CVPR, 2015. (http://arxiv.org/pdf/1501.05759)

How Far are We from Solving Pedestrian Detection?
Shanshan Zhang, Rodrigo Benenson, Mohamed Omran, Jan Hosang, Bernt Schiele. 
CVPR, 2016. (http://arxiv.org/pdf/1602.01237)

The code is based on Piotr Dollars matlab toolbox (http://vision.ucsd.edu/~pdollar/toolbox/doc/). 
Please make sure you are familiar with it before you start using our code.

We release two versions of implementation:
1) Checkerboards_LDCF_codebase: 
    this is an easy-to-use implementation, which enables experiments across different types of filters. 
    
2) Checkerboards_CVPR15_codebase: 
   this is a fast version we used for experiments in our CVPR15 paper.
  
For more details of their usage, please refer to the corresponding readme files in both folders.


Below are some instructions on preparing the Caltech pedestrian data:

1. Download data.
Caltech pedestrian dataset can be downloaded at:
http://www.vision.caltech.edu/Image_Datasets/CaltechPedestrians/datasets/USA/

The training data (set00-set05) consists of six training sets (~1GB each), 
each with 6-13 one-minute long seq files, along with all annotation information (see the paper for details). 
The testing data (set06-set10) consists of five sets, again ~1GB each.

2. Extract data.
Please download the evaluation and labeling code at:
http://www.vision.caltech.edu/Image_Datasets/CaltechPedestrians/code/code3.2.1.zip
and run 'dbExtract' to extract downloaded data to image frames and annotation text files. Please set skip=3 to obtain Caltech 10x data.

If you use this code, please kindly cite our papers:
@INPROCEEDINGS{Shanshan2015CVPR,
  Author = {Shanshan Zhang and Rodrigo Benenson and Bernt Schiele},
  Title = {Filtered Channel Features for Pedestrian Detection},
  Booktitle = {CVPR},
  Year = {2015}
 }
@INPROCEEDINGS{Shanshan2016CVPR,
  Author = {Shanshan Zhang and Rodrigo Benenson and Mohamed Omran and Jan Hosang and Bernt Schiele},
  Title = {How Far are We from Solving Pedestrian Detection?},
  Year = {2016},
  Booktitle = {CVPR}
 }
