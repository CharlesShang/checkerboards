load('../Checkerboards_CVPR15_codebase/models_Caltech/Checkerboards/CheckerboardsDetector.mat');
detector = acfModify_my(detector,'cascThr',-1,'cascCal',-0.1); % 24s, 300 boxes per image
% detector = acfModify_my(detector,'cascThr',-1,'cascCal', 0);     % 80s, 600 boxes per image
% detector = acfModify_my(detector,'cascThr',-1,'cascCal', -0.15); % 15s, 150 boxes per image
% detector = acfModify_my(detector,'cascThr',-1,'cascCal', -0.2);  % 13s, 3 boxes per image ~ around(can be used)
% detector = acfModify_my(detector,'cascThr',-1,'cascCal', -0.25); % 13s, 0 boxes per image ~ around(can be used)

%% some exmaples 
dataDir = '~/Work/CheckerBoards/data/Caltech/';
imgNms=bbGt('getFiles',{[dataDir 'test/images']});
I=imread(imgNms{1862}); 
tic, bbs=acfDetect_my(I,detector); toc
figure(1); im(I); bbApply('draw',bbs); pause(.1);
I=imread(imgNms{2588}); 
tic, bbs=acfDetect_my(I,detector); toc
figure(1); im(I); bbApply('draw',bbs); pause(.1);
% some exmaples end

vbbDir='~/Work/CheckerBoards/data/Caltech/';
testdataDir = '~/Work/CheckerBoards/data/Caltech/train/images/';
testgtDir = '~/Work/CheckerBoards/data/Caltech/train/annotations/';

pLoad={'lbls',{'person'},'ilbls',{'people'},'squarify',{3,.41}};

tstart = tic;
[miss,gt,dt]=sampleNegWidowsCaltech(vbbDir, detector, ...
     'reapply', 1, ...
      'detres','CheckerboardsWindows.txt','imgDir',testdataDir ,...
      'gtDir',testgtDir,'pLoad',[pLoad, 'hRng',[50 inf],...
      'vRng',[.65 1],'xRng',[5 635],'yRng',[5 475]],'show',2);
telapsed = toc(tstart);